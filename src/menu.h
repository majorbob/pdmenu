/* 
 * Copyright (c) 1995, 1996, 1997 Joey Hess (pdmenu@joeyh.name)
 * All rights reserved. See COPYING for full copyright information (GPL).
 */

/* This must be long enough for "selbarhi". */
extern char ScreenObjNames[NUMSCREENPARTS+1][LONGEST_OBJ_NAME+1]; 
extern int DESKTOP;
extern int TITLE;
extern int BASE;
extern int MENU;
extern int SELBAR;
extern int SHADOW;
extern int MENU_HI;
extern int SELBAR_HI;
extern int UNSEL_MENU;

void DrawMenu(Menu_Type *,int);
void SelMenuItem(Menu_Type *,int,int);
void FillMenu(Menu_Type *,int);
int DoMenu(Menu_Type *,int (),void ());
void CalcMenu(Menu_Type *);
Menu_Type *LookupMenu (const char *);
void SanityCheckMenus(void);
void RemoveMenu(Menu_Type *);

#define Q_KEY_EXIT 2
#define QUIT_EXIT 3

/*
 * Copyright (c) 1995-1999 Joey Hess (pdmenu@joeyh.name)
 * All rights reserved. See COPYING for full copyright information (GPL).
 */

char *unescape (char *, char);
char *pdstrtok (char *, char);

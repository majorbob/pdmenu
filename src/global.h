/*
 * All pdmenu code will want to include this file.
 */

/* 
 * Copyright (c) 1995-1999 Joey Hess (pdmenu@joeyh.name)
 * All rights reserved. See COPYING for full copyright information (GPL).
 */

#include "config.h"
#include "menutype.h"
#include "windowtype.h"

/* Global variables */

/* Pointer to linked list of all windows on the screen */
extern Window_List_Type *CurrentWindow;
/* Pointer to linked list of all available menus. */
extern Menu_Type *menus;
/* Pointer to the menu that will be modified by new rc commands. */
extern Menu_Type *current_rc_menu;

/* Boolean variables, 0=no, 1=yes */
extern int Use_Color;
extern int Q_Exits;
extern int Unpark_Cursor;
#ifdef GPM_SUPPORT
/* Was the mouse detected? */
extern int gpm_ok;
#endif
/* Set when the screen has resized */
extern int Want_Screen_Resize;
/* Use old-style look if set. */
extern int Retro;
/* Don't use high bit line drawing chars. */
extern int Lowbit;
/* Draw borders with spaces. */
extern int Borderspace;
/* Don't use 2 or 8 for up and down if set */
extern int Numeric;
/* Use superhot hot keys */
extern int Superhot;
